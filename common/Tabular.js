import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Promise } from 'meteor/promise';
global.Buffer = global.Buffer || require("buffer").Buffer;
let ObjectId = require('mongodb').ObjectId

const Tabular = {};

// let debug = {
//   debug : true,
//   log : function(...info){
//     if(this.debug){
//       console.log(info)
//     }
//   }
// }
// debug.debug = false

Tabular.tablesByName = {};

Tabular.Table = class {
  constructor(options) {
    if (!options) throw new Error('Tabular.Table options argument is required');
    if (!options.name) throw new Error('Tabular.Table options must specify name');
    if (!options.columns) throw new Error('Tabular.Table options must specify columns');
    if (!(options.collection instanceof Mongo.Collection
      || options.collection instanceof Mongo.constructor // Fix: error if `collection: Meteor.users`
    )) {
      throw new Error('Tabular.Table options must specify collection');
    }

    this.name = options.name;
    this.collection = options.collection;

    this.pub = options.pub || 'tabular_genericPub' + this.name;

    // By default we use core `Meteor.subscribe`, but you can pass
    // a subscription manager like `sub: new SubsManager({cacheLimit: 20, expireIn: 3})`
    this.sub = options.sub || Meteor;

    this.onUnload = options.onUnload;
    this.allow = options.allow;
    this.allowFields = options.allowFields;
    this.changeSelector = options.changeSelector;
    this.throttleRefresh = options.throttleRefresh;
    this.alternativeCount = options.alternativeCount;
    this.skipCount = options.skipCount;

    if (_.isArray(options.extraFields)) {
      const fields = {};
      _.each(options.extraFields, fieldName => {
        fields[fieldName] = 1;
      });
      this.extraFields = fields;
    }

    this.selector = options.selector;

    this.options = _.omit(
      options,
      'collection',
      'pub',
      'sub',
      'onUnload',
      'allow',
      'allowFields',
      'changeSelector',
      'throttleRefresh',
      'extraFields',
      'alternativeCount',
      'skipCount',
      'name',
      'selector'
    );

    Tabular.tablesByName[this.name] = this;

    if(Meteor.isServer){
      setPublications(this.name)
    }
  }
}

function setPublications(v){
  Meteor.publish('tabular_genericPub'+v, function (tableName, ids, fields) {
    check(tableName, String);
    check(ids, Array);
    check(fields, Match.Optional(Object));
  
    const table = Tabular.tablesByName[tableName];
    if (!table) {
      // We throw an error in the other pub, so no need to throw one here
      this.ready();
      return;
    }
  
    // Check security. We call this in both publications.
    if (typeof table.allow === 'function' && !table.allow(this.userId, fields)) {
      this.ready();
      return;
    }
  
    // Check security for fields. We call this only in this publication
    if (typeof table.allowFields === 'function' && !table.allowFields(this.userId, fields)) {
      this.ready();
      return;
    }
  
    
    if(table.options.joins){ 
      var agregate=[];
      // agregate.push({$match:{_id: {$in: ids}}});
      if(table.options.isObjectID){
        const objectIDS = ids.map(_id => {
          const id = ObjectId(_id)
          return id
        });
        agregate.push({$match:{_id: {$in: objectIDS}}});
      }else{
        agregate.push({$match:{_id: {$in: ids}}});
      }
      _.each(table.options.preAggregate,function(v,k){
        agregate.push(v);
      });
      _.each(table.options.joins,function(v,k){
        agregate.push({$lookup:_.pick(v,'from','localField','foreignField','as')});
        if(v.unwind){
          agregate.push({$unwind:{path:'$'+v.as,preserveNullAndEmptyArrays:true}});
        }
      });
      _.each(table.options.postAggregate,function(v,k){
        agregate.push(v);
      });
      if(table.options.joinNonReactive){
        // debug.log('no reactive join')
        const self = this
        const aggregateCursor = Promise.await(table.collection.rawCollection().aggregate(agregate).toArray())
        aggregateCursor.forEach(function (doc) {
          const id = typeof doc._id === 'object' ? doc._id.toString() : doc._id
          self.added(table.collection._name, id, doc);
        });
        this.ready()
      }else{
        // debug.log('yes reactive join')
        ReactiveAggregate(this, table.collection, agregate)
      }
    }else{
      return table.collection.find({_id: {$in: ids}}, {fields: fields});
    }
    // debug.log('second sub ready',Date())
  
    /*return table.collection.aggregate([{
      $match:{_id: {$in: ids}},
    }]);*/
    
  });
  
  Meteor.publish('tabular_getInfo'+v, function (tableName, selector, sort, skip, limit) {
    check(tableName, String);
    check(selector, Match.Optional(Match.OneOf(Object, null)));
    check(sort, Match.Optional(Match.OneOf(Array, null)));
    check(skip, Number);
    check(limit, Match.Optional(Match.OneOf(Number, null)));
  
    // debug.log('init publish',Date())
  
    const table = Tabular.tablesByName[tableName];
    if (!table) {
      throw new Error(`No TabularTable defined with the name "${tableName}". Make sure you are defining your TabularTable in common code.`);
    }
  
    // Check security. We call this in both publications.
    // Even though we're only publishing _ids and counts
    // from this function, with sensitive data, there is
    // a chance someone could do a query and learn something
    // just based on whether a result is found or not.
    if (typeof table.allow === 'function' && !table.allow(this.userId)) {
      this.ready();
      return;
    }
  
    selector = selector || {};
  
    // Allow the user to modify the selector before we use it
    if (typeof table.changeSelector === 'function') {
      selector = table.changeSelector(selector, this.userId);
    }
  
    // Apply the server side selector specified in the tabular
    // table constructor. Both must be met, so we join
    // them using $and, allowing both selectors to have
    // the same keys.
    if (typeof table.selector === 'function') {
      const tableSelector = table.selector(this.userId);
      if (_.isEmpty(selector)) {
        selector = tableSelector;
      } else {
        selector = {$and: [tableSelector, selector]};
      }
    }
    var agregate=[];
  
    const findOptions = {
      skip: skip,
      fields: {_id: 1}
    };
  
    // `limit` may be `null`
    if (limit > 0) {
      findOptions.limit = limit;
    }
  
    var tempSort={};
    // `sort` may be `null`
    if (_.isArray(sort)) {
      findOptions.sort = sort;
      _.each(sort,sort=>{
        tempSort[sort[0]]=sort[1]==="asc"?1:-1;
      });
    }
  
    let filteredRecordIds;
    let countRows=0;
    let filteredCursor;
    if(table.options.joins&&(!table.options.joinNonSearch)){
      //agregated IDs
      // debug.log('yes join')
      
      _.each(table.options.joins,function(v,k){
        agregate.push({$lookup:_.pick(v,'from','localField','foreignField','as')});
      });
      agregate.push({$match:selector});
      if(!_.isEmpty(tempSort)) agregate.push({$sort:tempSort});
      agregate.push({$skip:findOptions.skip});
      agregate.push({$limit:findOptions.limit});
      // filteredCursor = table.collection.find();
      filteredCursor = table.collection.find(selector, findOptions);

    //   let filteredCursorAgregated = table.collection.aggregate(agregate)
      let filteredCursorAgregated = Promise.await(table.collection.rawCollection().aggregate(agregate).toArray())
    //   console.log('cursor', filteredCursorAgregated)

      filteredRecordIds = filteredCursorAgregated.map(doc => {
        return typeof doc._id === 'object' ? doc._id.toHexString() : doc._id
      });
      //countRows = filteredCursorAgregated.length;
      countRows = filteredRecordIds.length + skip + 1;
      //debug.log(filteredRecordIds);
    }else{
      // debug.log('non join')
      //IDs the regular way
      filteredCursor = table.collection.find(selector, findOptions);
      filteredRecordIds = filteredCursor.map(doc => {
        return typeof doc._id === 'object' ? doc._id.toHexString() : doc._id
      });
      countRows = filteredCursor.count();
    }
  
    // If we are not going to count for real, in order to improve performance, then we will fake
    // the count to ensure the Next button is always available.
    const fakeCount = filteredRecordIds.length + skip + 1;
  
    let recordReady = false;
    let updateRecords = () => {
      let currentCount;
      if (!table.skipCount) {
        if (typeof table.alternativeCount === 'function') {
          currentCount = table.alternativeCount(selector);
        } else {
          currentCount = countRows;
        }
      }
  
      // From https://datatables.net/manual/server-side
      // recordsTotal: Total records, before filtering (i.e. the total number of records in the database)
      // recordsFiltered: Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
  
      const record = {
        ids: Array.isArray(filteredRecordIds) ? filteredRecordIds : [],
        // ids: filteredRecordIds,
        // count() will give us the updated total count
        // every time. It does not take the find options
        // limit into account.
        recordsTotal: table.skipCount ? fakeCount : currentCount,
        recordsFiltered: table.skipCount ? fakeCount : currentCount
      };
  
      if (recordReady) {
        //debug.log('changed', tableName, record);
        this.changed('tabular_records', tableName, record);
      } else {
        //debug.log('added', tableName, record);
        // console.log('added', tableName, record, filteredRecordIds)
        this.added('tabular_records', tableName, record);
        recordReady = true;
      }
    }
  
    if (table.throttleRefresh) {
      // Why Meteor.bindEnvironment? See https://github.com/aldeed/meteor-tabular/issues/278#issuecomment-217318112
      updateRecords = _.throttle(Meteor.bindEnvironment(updateRecords), table.throttleRefresh);
    }
  
    updateRecords();
  
    this.ready();
    // debug.log('ready',Date())
  
    
    // Handle docs being added or removed from the result set.
    let initializing = true;
    const handle = filteredCursor.observeChanges({
      added: function (id) {
        if (initializing) return;
  
        //debug.log('ADDED');
        if(table.options.joins){
          var agregate=[];
          _.each(table.options.joins,function(v,k){
            agregate.push({$lookup:_.pick(v,'from','localField','foreignField','as')});
          });
          agregate.push({$match:selector});
          if(!_.isEmpty(tempSort)) agregate.push({$sort:tempSort});
          agregate.push({$skip:findOptions.skip});
          agregate.push({$limit:findOptions.limit});
        //   const filteredCursor = table.collection.aggregate(agregate)
          const filteredCursor = Promise.await(table.collection.rawCollection().aggregate(agregate).toArray())
          filteredRecordIds = filteredCursor.map(doc => doc._id);
          countRows = filteredCursor.length;
        }else{
          filteredRecordIds.push(id);
        }
        updateRecords();
      },
      removed: function (id) {
        //debug.log('REMOVED');
        if(table.options.joins){
          var agregate=[];
          _.each(table.options.joins,function(v,k){
            agregate.push({$lookup:_.pick(v,'from','localField','foreignField','as')});
          });
          agregate.push({$match:selector});
          if(!_.isEmpty(tempSort)) agregate.push({$sort:tempSort});
          agregate.push({$skip:findOptions.skip});
          agregate.push({$limit:findOptions.limit});
        //   const filteredCursor = table.collection.aggregate(agregate)
          const filteredCursor = Promise.await(table.collection.rawCollection().aggregate(agregate).toArray())
          filteredRecordIds = filteredCursor.map(doc => doc._id);
          countRows = filteredCursor.length;
        }else{
          // _.findWhere is used to support Mongo ObjectIDs
          filteredRecordIds = _.without(filteredRecordIds, _.findWhere(filteredRecordIds, id));
        }
        updateRecords();
      }
    });
    initializing = false;
  
    // It is too inefficient to use an observe without any limits to track count perfectly
    // accurately when, for example, the selector is {} and there are a million documents.
    // Instead we will update the count every 10 seconds, in addition to whenever the limited
    // result set changes.
    const interval = Meteor.setInterval(updateRecords, 10000);
  
    // Stop observing the cursors when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    this.onStop(() => {
      Meteor.clearInterval(interval);
      handle.stop();
    });
  });
}

export default Tabular;
